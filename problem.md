---
layout: page
title: Problem
date: 2023-09-13
---
AI will have some potential negative consequences for humanity, among others: losing control over AI, power inequality and people feeling useless as a result of losing their jobs.

It is important that people take the effort to minimize the negative AI impact and discuss how to minimize it.

Currently, that effort is made mostly by people at elite AI companies, academics and government people.

In my opinion, everyone should have the opportunity to contribute to this discussion and be financially rewarded and recognized proportionally to the value of their contribution.

In my opinion, everyone should have that opportunity because:
1. Minimizing negative AI impact is a new field and coming up with new insights doesn't necessarily require a lot of prior knowledge.
2. AI will affect all areas of life. A person specializing in computer science might not foresee something that can happen in area where they don't have expertise. For example, a computer scientist at AI company might not think about AI being used to create a biological weapon because they are not interested in biology. It is therefore important so that all people (with different backgrounds, experiences and accumulated data) have a voice in this discussion.
3. It's something that requires unique insights. Unique insights are not something that you go to college and you are guaranteed to come up with them. The best way to gather unique insights is to ask as many people as possible.

In my opinion, it is important to reward people's effort because:
1. So that the people have motivation to do that and focus on doing things where they can contribute the most value. For example, many people focus on creating AI products because they can make more money there. But they could do a greater impact by ensuring that AI will play out well (but they are not paid for that).
2. So that the people have time to do that (if you don't get paid for that, then you don't have time for doing that because you need to spend your time doing something that will pay off your bills).

The problem is that currently the world doesn't work like that. Currently, there is no reward for normal people for contributing insights on how to minimize the negative AI impact.
