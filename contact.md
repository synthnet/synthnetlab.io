---
title: "Contact"
permalink: "/contact/"
layout: page
---

I'm open for all kinds of collaboration.

For contact, you can use the following form:

<form action="https://formspree.io/f/xyyqgwkb" method="POST">
    <label for="name">Your Name:</label><br>
    <input type="text" id="name" name="name"><br>
    <label for="email">Your Email:</label><br>
    <input type="email" id="email" name="_replyto"><br>
    <label for="message">Your Message:</label><br>
    <textarea id="message" name="message" rows="10" cols="70"></textarea><br>
    <input type="submit" value="Send">
</form>

