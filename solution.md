---
layout: page
title: Solution
date: 2013-09-13
---
1. The following is the idea of the SynthNet project. The idea might evolve. We promise only the things that we say that we promise and nothing else. However, we have an incentive to evolve the idea only in a way that serves the people because the project is funded by people, so we won't receive any more funding if it doesn't serve the people.
2. SynthNet is an online community/forum.
3. Goal: minimize the negative AI impact, in a socially beneficial way.
4. People can add posts that describe insights aiming to minimize negative AI impact.
5. People can vote on posts (the intention is that people will vote on them proportionally to the value of the insight).
6. People are financially rewarded for their posts/insights. The more votes their post gets, the more reward they get.
7. The project and the rewards are funded by individual people (but companies, organizations and institutions are also welcome).
8. When people fund the project, they receive votes that they can cast on posts (a person can cast more than 1 vote on a post). The more money they contribute, the more votes they have to distribute.
9. When we start to raise money, we are going to make a promise that the funding that the project is going to raise will be spent in the following way:
	1. About 75% of the raised funding will be spent of the rewards for people making a contribution to minimize the negative AI impact.
	2. About 25% of the raised funding will be spent on project development, maintenance, promotion and other costs.
	3. If the project fails, then the spending on the rewards for posters will go somewhere else, but it won't be spent in a way that significantly benefits the creators so that the creators don't have an incentive to make the project fail.
10. The posts on the main page are sorted by the amount of votes they got, but the recent votes count more.
11. The entire budget for rewards is distributed towards posters, proportionally to how much votes they have received. But there's an exception: the reward that a poster gets can't exceed `2 * v`, where `v` is the amount of money that it takes to buy the votes that were cast on the poster's posts. That is a protection against gaming the system by voting on your own posts (or using friends/relatives to do that). If that rule leads to a situation that there is some remaining money in the budget for rewards, then the creator(s) of SynthNet will decide how the remaining money will be distributed among the posters.
13. At the beginning SynthNet is an online forum, but later it's planned to be more than just a forum - an online community that doesn't just discuss things, but also does things (take some actions, instead of only exchanging viewpoints).
14. There will be a "hall of fame" page that will simply assign credit to the best contributors (those who collectively received the highest number of votes).
15. The community and platform is going to be improved iteratively.
16. The next improvement might be that people are going to be rewarded for voting early on posts that later became strongly upvoted, so people have incentive to vote and think deeply about what they cast votes for.
17. In the future, SynthNet is planned to be a community that doesn't just exchange viewpoints but also does things (take actions).
18. SynthNet is planned to become a self-improving / autonomous community - the platform will be created, improved and maintained by the community itself, to an increasing extent.