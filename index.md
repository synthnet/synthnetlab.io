---
layout: page
title: "Home"
---
I'm proposing the SynthNet project - an online community aiming to minimize the negative impact of AI, in a socially beneficial way.

Please read this if you'd like to learn how you can minimize the negative impact of AI for yourself and for others, in a way that is beneficial to you.

# Table of contents

 - [Problem](#problem)
 - [Solution](#solution)
 - [Show your support](#show-your-support)
 - [Stay notified](#stay-notified)
 - [Feedback](#feedback)

# Problem

The potential negative AI impacts include: losing control over AI, power inequality, and people feeling useless (of course, there are more).

Currently, there is no way for an ordinary person to get rewarded for contributing to minimizing the negative AI impact (especially by sharing their viewpoints/insights). It's important to reward that contribution so that people have the incentive and the resources (including time) to contribute.

[Read more about the problem]({% link problem.md %}) 

# Solution

At the beginning, the project will be an online forum for insights about how to minimize the negative impact of AI. People will be able to write posts with their insights. The insights will be financially rewarded, proportionally to their value.

The community will propose insights on how to minimize the negative impact of AI, but also how to develop the project further so that it can achieve its purpose better.

![Platform screenshot](/assets/img/platform.png)

How will the project be funded? Who will pay for the rewards, project development and maintenance?

It will be funded from the money from people (but also companies, organizations and institutions). Why would people contribute money to it? Because AI is something that can potentially impact anyone negatively, so everyone gets some benefit out of minimizing the negative impact. If the entire world agreed that they will spend some amount of their money on the effort to minimize the negative impact, it would be beneficial for any person on average.

However, that deal is beneficial to everyone, assuming that everyone else will participate in it. If there's a handful of people who choose to fund a project that minimizes the negative AI impact, the benefit for those people might not outweigh the cost, since the benefit of the project is distributed among the entire world.

To solve this, people will be able to make conditional contributions - they can give X money under the condition that the project raises Y from others. It's like saying "I'll give $10 if others (or at least enough people) also pay $10".

The project will also be funded by investors because reaching a sufficient number of people might be too costly.

People who fund the project can vote on posts. The rewards for contributors are based on the votes ratio.

[Read more about the solution]({% link solution.md %})

# Show your support {#show-your-support}

At the moment, we want to see if people like this idea and want to support it.

Fill out [this form](https://forms.gle/NmxmNg5Paa7PPLUa8) to show you're interested and want to give money to the project in the future.

# Stay notified

Fill out [this form](https://forms.gle/wbPA9ffuoRmV2sP26) to stay notified (you'll be able to unsubscribe).

# Feedback

Submit [this form](https://forms.gle/vqboqbh29X6DTLqY7) to tell us how we can improve the project for you or why the project might be a bad idea.